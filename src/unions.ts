// Type Alias
type Combinable = number | string;
//Literal Type
type ConversionDescriptor = "as-number" | "as-text";

function combine(
	input1: Combinable,
	input2: Combinable,
	// Literal Type used in the context of a Union Type
	resultType: ConversionDescriptor
) {
	let result;
	if (
		(typeof input1 === "number" && typeof input2 === "number") ||
		resultType === "as-number"
	) {
		result = +input1 + +input2;
	} else {
		result = input1.toString() + input2.toString();
	}
	return result;
}

const combineAges = combine(30, 26, "as-number");
const combineNames = combine("Alex", "Sara", "as-text");
const combineStringAges = combine("30", "26", "as-number");
console.log(combineAges);
console.log(combineNames);
console.log(combineStringAges);

/*
    Union Type:
     If we have some place in our application, be that
     a parameter of a function or const/variable that we are using
     somewhere, where we accept two different types of values, a Union
     Type can help us.
     To tell TS that we are fine with either a number
     or string for values, you use the pipe symbol , "|" , then the other
     type you want to accept.You can accept as many types as you want.

    Literal Types:
     Are types where you don't just say that certain parameter, const or variable
     should hold a number or a string, but you are very clear about the exact value
     it should hold. 

    Type Alias:
     A TS feature which lets you store your Union Types. You create a Type Alias,
     typically before you use it, with the 'type' keyword. The 'type' keyword is not
     known to JS, but is known to TS. After the type keyword, you put your Alias name.
     After that, you put an equals sign and then your Union type or Literal type.

   
     */
