type Admin1 = {
	name: string;
	privileges: string[];
};

type Employee12 = {
	name: string;
	startDate: Date;
};

// Intersection type that combines Admin and Employee
type ElevatedEmployee12 = Admin & Employee12;

const e112: ElevatedEmployee = {
	name: "Alex",
	privileges: ["All Known"],
	startDate: new Date(),
};

function add22(a: Combinable, b: Combinable) {
	// Below is a Type Guard because it allows us to utilize the flexibility
	// Union types gives and still insure that our code runs correctly at runtime.
	if (typeof a === "string" || typeof b === "string") {
		return a.toString() + b.toString();
	}
	return a + b;
}

type UnknownEmployee = Employee | Admin;

function printEmployeeInfos(emp: UnknownEmployee) {
	// Below are more Type Guards
	if ("privileges" in emp) {
		console.log("Privilege: " + emp.privileges);
	}
	if ("startDate" in emp) {
		console.log("Start Date: " + emp.startDate);
	}
	console.log("Name: " + emp);
}

class Car {
	drive() {
		console.log("Driving...");
	}
}

class Truck {
	drive() {
		console.log("Driving with 4wheel drive!...");
	}

	loadCargo(amount: number) {
		console.log("Loading cargo " + amount);
	}
}

type Vehicle = Car | Truck;

const v12 = new Car();

const v22 = new Truck();

function useVehicles(vehicle: Vehicle) {
	vehicle.drive();
	// More elegant way of using type guard using instanceof
	if (vehicle instanceof Truck) {
		vehicle.loadCargo(666);
	}
}

useVehicles(v12);
useVehicles(v22);
printEmployeeInfos(e112);

/*
 Type Guards:
    help us with Union types. While it is nice to have the Flexibility
    that Union types offer, often times you need to know which exact type
    you are getting at runtime. 

    When working with classes, you can use another type of Type Guard, the 
    instanceof Type Guard. You can not use instanceof with interfaces because 
    interfaces are not compiled to any JS code, therefore we cannot use them at runtime.

    In the end, Type Guards are just an idea, an approach, of checking if a certain property
    or method exists before you try to use it. 
        For objects, it can be done using instanceof or "in".
        For other types, you can use typeof
        
*/
