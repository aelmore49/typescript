function merge2<T extends object, U extends object>(objA: T, objB: U) {
	return Object.assign(objA, objB);
}

/*

Constraints:
    For Generic Types, you can set certain type constraints
    your object is based on. You do this in an Generic Type
    in the angle brackets after the type you want to constraint.

*/
