type Admin = {
	name: string;
	privileges: string[];
};

type Employee = {
	name: string;
	startDate: Date;
};

// Intersection type that combines Admin and Employee
type ElevatedEmployee = Admin & Employee;

const e1: ElevatedEmployee = {
	name: "Alex",
	privileges: ["All Known"],
	startDate: new Date(),
};
console.log(e1);
/*


    Intersection Types:
        allow us to combine other types. 

*/
