function easyUnpack(values: any[]): any[] {
	// your code here
	const tup: [number, number, number] = [
		values[0],
		values[2],
		values[values.length - 2],
	];
	return tup;
}

// console.log("Example:");
// console.log(easyUnpack([1, 2, 3, 4, 5, 6, 7, 9]));
// console.log(easyUnpack([1, 1, 1, 1]));
// console.log(easyUnpack([6, 3, 7]));

// These "asserts" are used for self-checking
easyUnpack([1, 2, 3, 4, 5, 6, 7, 9]); //, [1, 3, 7]);
easyUnpack([1, 1, 1, 1]); //, [1, 1, 1]);
easyUnpack([6, 3, 7]); //, [6, 7, 3]);

console.log("Coding complete? Click 'Check' to earn cool rewards!");

/* ----------------------------------------------------------------- */

/*

You are the beginning of a password series. Every mission will be based on the previous one. Going forward the missions will become slightly more complex.

In this mission you need to create a password verification function.

Those are the verification conditions:

the length should be bigger than 6.
Input: A string.

Output: A bool.

Example:

isAcceptablePassword('short') == false
isAcceptablePassword('muchlonger') == true
isAcceptablePassword('ashort') == false

*/

function isAcceptablePassword(password: string): boolean {
	// your code here
	return password.length > 6 ? true : false;
}

// These "asserts" are used for self-checking
// console.log(isAcceptablePassword("short")); // false;
// console.log(isAcceptablePassword("muchlonger")); //true;
// console.log(isAcceptablePassword("ashort")); // false;

/* ----------------------------------------------------------------- */

/*

You have a positive integer. Try to find out how many digits it has?

Input: A positive Int

Output: An Int.

Example:

numberLength(10) == 2
numberLength(0) == 1
numberLength(4) == 1
numberLength(44) == 2
1
2
3
4

*/

function numberLength(value: number): number {
	// your code here
	return value.toString().length;
}

// These "asserts" are used for self-checking
// console.log(numberLength(10)); //, 2);
// console.log(numberLength(0)); //, 1);
// console.log(numberLength(4)); //, 1);
// console.log(numberLength(44)); //, 2);

/* ----------------------------------------------------------------- */

/*

Try to find out how many zeros a given number has at the end.

Input: A positive Int

Output: An Int.

Example:

endZeros(0) == 1
endZeros(1) == 0
endZeros(10) == 1
endZeros(101) == 0

*/

function endZeros(value: number): number {
	// your code here
	let numberOfZeros: number = 0;
	const numberString: string = value.toString();
	const lastElement: string = numberString.charAt(numberString.length - 1);
	const secondToLastElement: string = numberString.charAt(
		numberString.length - 2
	);
	if (lastElement === "0") {
		numberOfZeros += 1;
	}
	if (secondToLastElement === "0" && lastElement === "0") {
		numberOfZeros += 1;
	}

	return numberOfZeros;
}

// These "asserts" are used for self-checking
console.log(endZeros(0)); //, 1);
console.log(endZeros(1)); //, 0);
console.log(endZeros(10)); //, 1);
console.log(endZeros(101)); //, 0);
console.log(endZeros(245)); //, 0);
console.log(endZeros(100100)); //, 2);
