// Version 1 of Type Casting
const input = <HTMLInputElement>document.getElementById("user-input");
input.value = "hi";

// Version 2 of Type Casting
const input2 = document.getElementById("user-input-2") as HTMLInputElement;
input2.value = "hi there!";

/*

Type Casting:

    using an "!" will tell TS that the expression in front of it will never
    yield a null value. 
    Ex: const input = document.getElementById("user-input")! as HTMLInputElement;

    Helps you tell TS that some value is of specific type where 
    TS is not able to detect it on its own, but you as a developer
    know that it will be the case.

    There are two ways of Type Casting: 
        1. We add "<>" in front of the thing we want to convert. Between
        the "<>" the type of the thing 

        2. To not clash with the React JSX syntax, which also uses the "<>",
        the TS Team provides an alternative version of Type Casting. You can
        add "as" keyword after the thing you want to convert and then the type
        you want to cast it to. 

*/
