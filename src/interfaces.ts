interface AddFn {
	(a: number, b: number): number;
}

interface Named {
	readonly name: string;
	outputName?: string;
}

interface Greetable extends Named {
	greet(pharse: string): void;
}

class Person implements Greetable {
	name: string;
	age = 30;
	constructor(n: string) {
		this.name = n;
	}
	greet(pharse: string) {
		console.log("hi", pharse);
	}
}

const addThatShit = (a: number, b: number): number => {
	return a + b;
};

let getMyAdded: AddFn = addThatShit;

console.log(getMyAdded(69, 420));

let user1: Greetable;

user1 = {
	name: "Alex",
	greet(phrase: string) {
		console.log("Hi!", phrase);
	},
};

/*
    Interfaces:
        Describes the structure of an object.
        We can use an interface to describe
        how an object should look like.
        
    * We create an interface with the "interface" keyword,
        which only exists in TS.
        In our interface, we can now define how a class should look like. It is not a 
        blueprint, just somewhat of a custom type.
        So in an interface you can add names
        and types of properties, what you don't have is their concrete values.
        You can also add methods, just the structure, not the concrete values. 
        So for methods, you add arguments and their types in the parenthesis
        and its return type.
        We can now use our instance to type check a variable. 

    * You can implement an interface within a class. To implement an interface with a
        class, you add the "implements" keyword after the class name. You can implement
        more than one interface by separating them with a comma. 
        An interface and a custom type are not exactly the same. 

        **** Interfaces are used for object structure only. *****

    * Inside of an interface, you can add "readonly" to a property, which makes it clear
        that the property must only be set once, cannot be changed and is read only. 
        Interfaces can also extend from other interfaces using the "extends" keyword. 
        You can extend multiple interfaces using a comma.

    * Interfaces can also be used to describe the structure of a function.
        To do so, you put the "interface" keyword in front of your function name.
        Within the function's
        curly braces, you create your method with the parenthesis,
        its arguments and types. Then after the closing
        parenthesis, you put a colon and its return type. 

    * You can also define optional properties in interfaces. 
        To create an optional property on an interface, you create a property as normal but
        put a "?" directly after its name. You can also define methods arguments as optional using the "!" directly
        after the method name.
*/
