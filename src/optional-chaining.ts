const fetchedUserData = {
	id: "u1",
	name: "Alex",
	job: {
		title: "CEO",
		description: "Global Tech",
	},
};

console.log(fetchedUserData?.job?.title);
/*

Optional Chaining:
    helps us safely access nested properties and nested objects in our 
    object data, and if the thing in front of the "?" is undefined, it will not
    access the thing there after and will not throw a runtime error. 


*/
