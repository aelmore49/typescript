const userInputNull = "";

const storedData = userInputNull ?? "Default for real null or undefined values";

console.log(storedData);
/* Nullish Coalescing:

    This feature helps us deal with nullish data.
    Let's say you are getting data from a backend server and you don't know if the
    data that is being returned to you will be null or not, and you want to do 
    something specific with the data if it is a true null value, then you could use
    Nullish Coalescing. The Nullish Coalescing operator is represented with "??".
    This operator will check if a value is really null or undefined, not just an 
    empty string. 

*/
