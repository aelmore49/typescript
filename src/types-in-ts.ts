const person: {
	name: string;
	age: number;
	hobbies: string[];
	// Tuple Type used with object property value
	roles: [number, string];
} = {
	name: "Alex",
	age: 35,
	hobbies: ["Tennis", "Web Development"],
	roles: [2, "author"],
};

let favoriteActivities: string[];
favoriteActivities = ["Golf"];

console.log(person.name);

for (const hobby of person.hobbies) {
	console.log(hobby.toLowerCase());
}

enum Role {
	ADMIN = 69,
	READ_ONLY,
	AUTHOR,
}

const person2 = {
	name: "Alex",
	age: 35,
	hobbies: ["Tennis", "Web Development"],
	roles: Role.ADMIN,
};

// Tuple Type used with a const
const employee: [number, string] = [1, "Alex"];

/*
Tuple type: is a fixed length and fixed type array
 Special construct that TS understands. They tell TS that
 you want special array with an exact length and type. 
 The array method "push", is an acceptation which is allowed in Tuples

 Enum type: this is the idea of having a couple of specific identifiers, global constants
 that you might be working with in your app, that you want to represent as numbers but
 to which you want to assign a human readable label. 
 The Enum type, which is added by TS, gives you an enumerated global constant 
 identifiers. 

 Any type: Must flexible type in TS. This means you can store any kind of value in there
 Avoid using Any whenever it is possible. 


*/
