// Decorator Function
function Loggers(constructor: Function) {
	console.log("Logging...");
	console.log(constructor);
}
// Decorator Factory
function Logger(logString: string) {
	// In terms of this file's Decorator Factories,
	// The below console log will execute first since it is outside of the
	// this Decorator Factory's return function
	console.log("Logger Decorator Outside of function");

	return function (constructor: Function) {
		console.log(logString);
		console.log(constructor);
	};
}
// Another Decorator Factory
function WithTemplate(template: string, hookId: string) {
	console.log("WithTemplate Decorator Outside of function");
	return function (constructor: any) {
		console.log("WithTemplate Decorator within the function");
		const hookEl = document.getElementById(hookId);
		const p = new constructor();
		if (hookEl) {
			hookEl.innerHTML = template;
			hookEl.querySelector("h1")!.textContent = p.name;
		}
	};
}

// "@" symbol that points to the Decorator

// Regular Decorator call that points to Decorator which takes the one argument,
// being a constructor function.
@Loggers
// Decorator Factory call that points to a Decorator Factory returns a
// a function that return the constructor function. Doing this, allows us
// to configure the arguments that are being passed in.
@WithTemplate("<h1> YO GURL!</h1>", "app")
class Person420 {
	name = "Alex The Great!";

	constructor() {
		console.log("Creating a new Person!");
	}
}

const per2 = new Person420();

console.log(per2);

// Example of adding a Decorator to a  property
function Logger2(target: any, propertyName: string | Symbol) {
	console.log("Property Decorator");
	console.log({
		"Target Property:": target,
		"Property Name:": propertyName,
	});
}

// Example of adding a Decorator to an accessor
function Logger3(target: any, name: string, descriptor: PropertyDescriptor) {
	console.log("Accessor Decorator");
	console.log({
		"Target Property:": target,
		"Property Name:": name,
		"Descriptor:": descriptor,
	});
}

// Example of adding a Decorator to a method
function Logger4(
	target: any,
	name: string | Symbol,
	descriptor: PropertyDescriptor
) {
	console.log("Method Decorator");
	console.log({
		"Target Property:": target,
		"Method Name:": name,
		"Descriptor:": descriptor,
	});
}

// Example of adding a Decorator to a parameter
function Logger5(target: any, name: string | Symbol, position: number) {
	console.log("Parameter Decorator");
	console.log({
		"Target Property:": target,
		"Method Name:": name,
		"Position:": position,
	});
}

class ProductClass {
	@Logger2
	title: string;

	private _price: number;

	@Logger3
	set price(val: number) {
		if (val > 0) {
			this._price = val;
		} else {
			throw new Error("Price has to be a positive value!");
		}
	}

	constructor(t: string, p: number) {
		this.title = t;
		this._price = p;
	}

	@Logger4
	getPriceWithTax(@Logger5 tax: number): number {
		return this._price * (1 + tax);
	}
}

/*

    Decorators:
       * Decorators are a particularly well suited instrument for writing code
            which is then easier for other developers to use. With Decorators we can
            ensure that our classes, methods, parameters and accessors get used correctly.   
       * A Decorator, in the end, is just a function, a function
            that you apply to something, for example, to a class, in 
            a certain way. The common convention is to start your Decorator
            name with a capitol letter. 
       * Decorators take in arguments, how many arguments it should take depends on 
            where you use it. You can add more than one Decorator. 
       * You can also define a "Decorator Factory" that returns a Decorator function, but 
            allows us to configure it when we assign a Decorator to something. When you want to apply
            your Decorator Factory, you have to execute it. Doing this will allow you to add arguments,
            as many as you want, to your Decorator Factory, which you can then configure. To tell
            TS that you are not interested in the constructor argument, you can use an "_" in its place.
        * Adding a Decorator to a class:
            you do so by adding a "@" symbol above the class and then your function. The
            "@" symbol is a special identifier that TS recognizes and then the thing 
            directly after the "@" should point at the function, which should be your 
            decorator. For a Decorator that you add to a class, you get one argument,
            which is the target of Decorator which is our constructor function. 
             Decorators on classes execute when your class is defined, not when it is instantiated.
        * When having more than one Decorators attached to a class
            or something else, the order in which the Decorator's Factory's return function is executed is
            from bottom up, meaning the Decorator's Factory that is directly above your class will execute
            its return function first and then the Decorator Factory(s) that are directly above IT will execute its return function and so on. 
            Now the opposite applies to the execution order of the Decorator Factory code that is outside of and before its return function.
       * Adding a Decorator to a class's property:
            If you add a Decorator to a class's property, the Decorator receives two arguments:
            The first argument is the target property. If the property that you are adding the
            Decorator to is an instance property, then the Decorator will be the prototype of the
            object that you created. If you add a Decorator to a static property, then it would
            refer to the constructor function instead.
       * Adding Decorators to accessors : 
            If you add a Decorator to an accessor, it will receive three arguments: The target,
            the name of the accessor and the property descriptor.
       * Adding Decorators to methods:
            Method Decorators receive three arguments: The target,
            the name of the method and the property descriptor.
        * Adding Decorators to parameters:
            Parameter Decorators receive three arguments: The target,
            the name of the method and the position of the argument.


*/
