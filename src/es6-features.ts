const userName1 = "Alex";
let age = 35;

// function add(a:number,b:number){
//     let result;
//     result = a+b;
//     return result;
// }

// console.log(result)

const add1 = (a: number, b: number = 1) => a + b;

const printOutput1 = (output: string | number = 2) => console.log(output);

const button = document.querySelector("button");

if (button) {
	button.addEventListener("click", (event) => {
		console.log(event);
	});
}

const hobbies1 = ["Tennis", "Coding"];

const activeHobbies = ["Running"];

activeHobbies.push(...hobbies1);
// console.log(activeHobbies);

const person22 = {
	firstName: "Alex",
	myAge: 35,
};

const copiedPerson = { ...person22 };

// console.log(copiedPerson);

const add3 = (...values: number[]) => {
	return values.reduce((cur: number, curVal: number) => {
		return cur + curVal;
	}, 0);
};

// console.log(add3(55, 66));

const [hobby1, hobby2, ...leftover] = hobbies1;

// console.log(hobby1);

const { firstName, myAge } = person22;
