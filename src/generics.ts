// // Generic Type Array
const names: Array<string> = ["Alex", "Sara"];

// Generic Type Promise
const promise: Promise<string> = new Promise((resolve, reject) => {
	let x = 0;
	// some code (try to change x to 5)
	if (x == 0) {
		resolve("Lets go!");
	}
	reject("Did not work!");
});

promise.then((data) => {
	console.log(data);
	data.toUpperCase;
});

// Generic Type Function
function merge<T, U>(objA: T, objB: U) {
	return Object.assign(objA, objB);
}

const mergeObj = merge({ name: "Alex" }, { age: 35 });
console.log(mergeObj.age);

// Interface that guarantees a length
interface Lengthy {
	length: number;
}
// function that extends Lengthy interface
// Set function to return an array, with first element being of type T
// and second element being
function countAndPrint<T extends Lengthy>(element: T): [T, string] {
	let descriptionText: string = "Got no value";
	if (element.length === 1) {
		descriptionText = "Got 1 element.";
	} else if (element.length > 1) {
		descriptionText = `Got a value of ${element.length} for this element's length.`;
	}

	// Return Tuple
	return [element, descriptionText];
}

const coding: Array<string> = ["JavaScript", "CSS", "Java", "AEM"];

console.log(countAndPrint("hey gurl!"));
console.log(countAndPrint(coding));

function extractAndConvert<T extends object, U extends keyof T>(
	obj: T,
	key: U
) {
	return obj[key];
}

extractAndConvert({ name: "Alex" }, "name");

class DataStorage<T extends string | number | boolean> {
	private data: T[] = [];

	addItem(item: T) {
		this.data.push(item);
	}

	removeItem(item: T) {
		this.data.splice(this.data.indexOf(item), 1);
	}

	getItems() {
		return [...this.data];
	}
}

const textStorage = new DataStorage<string>();
textStorage.addItem("Alex");
textStorage.addItem("Elmore");
console.log(textStorage.getItems());

const numberStorage = new DataStorage<number>();

// Generic Utility Types
interface CourseGoal {
	title: string;
	description: string;
	completeUntil: Date;
}

// Built In Partial Utility Type
function createCourseGoal(
	title: string,
	description: string,
	date: Date
): CourseGoal {
	let courseGoal: Partial<CourseGoal> = {};
	courseGoal.title = title;
	courseGoal.description = description;
	courseGoal.completeUntil = date;
	return courseGoal as CourseGoal;
}

// Built In Read Only Utility Type

const myNames: Readonly<string[]> = ["Alex", "Elmore"];
names.push("Robert");

/*

    Generics:
     A Generic Type is a type that is connected with some other type and is 
     really flexible regarding which exact type that other type is. 
     
     A Generic Type for an Array is specified using the "<>" and then the type of the 
     array inside the "<>". 
     Ex: Array<string>
     
     Another Generic Type which is built into TS is the Promise Type. 

    Generic Type Function: 
       We can turn a function into a generic function by adding
       angle brackets after the function name, then we define two 
       identifiers, typically you start with "T" and then you say
       that this generic type is available in the function. Now say your
       function has two arguments that you don't know the type of, then you can
       accept a second argument inside the angle brackets, separated by a 
       comma. Doing this will yield us a lot. Now TS will infer that our generic
       function will return the intersection of our two arguments inside of our
       generic function's angle brackets. Now, if we store the function in a const,
       TS will understand that what we store in our generic function is the intersection
       of its two arguments and it can now infer it. 

*/
