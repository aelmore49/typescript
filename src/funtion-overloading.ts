// function overload as a number
function added(a: number, b: number): number;

// function overload as a string
function added(a: string, b: string): string;

// function overload with a string and a number
function added(a: string, b: number): string;

function added(a: Combinable, b: Combinable) {
	if (typeof a === "string" || typeof b === "string") {
		return a.toString() + b.toString();
	}
	return a + b;
}

const result = added(1, 5);

const result2 = added("Alex", "Elmore");

/*

Function Overloads:
    a feature that allow us to have multiple ways of calling a function
    with different parameters. 


*/
