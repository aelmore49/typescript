let userInput: unknown;
let userName: string;

userInput = 5;
userInput = "Alex";
if (typeof userInput === "string") {
	userName = userInput;
}

function generateError(message: string, code: number) {
	throw {
		message: message,
		errorCode: code,
	};
}
generateError("An error occurred!", 500);

/*

 Unknown Type:
    We can store any value in there without getting errors.
    However, the Unknown Type is different from the Any Type.
    Unknown is more restrictive than Any. 
    * With Unknown, if you have
    a variable or const with the Unknown type, and you have another
    variable or const with a string type, and you want to reassign this
    variable to your variable with the Unknown type, TS will throw an
    error stating "let userName: string Type 'unknown' is not assignable
    to type 'string' " . If you switch unknown to any, the error will go
    away because any is the most flexible type in TS. Unknown is more 
    restrictive than Any. 
    * With Unknown, we have to
    first of all check what type is currently stored in the const or
    variable with the Unknown Type before we reassign it to a different
    type. Therefore, Unknown is a better choice than Any if you can tell
    what exact type a variable or const will be, but I know what I want
    to do with it eventually. 

 Never Type:
    The Never Type is another type that functions can return. The Never 
    Type can be used to make it clear that a function will never return
    anything. 

*/
