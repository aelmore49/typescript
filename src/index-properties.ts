interface ErrorContainer {
	[prop: string]: string;
}

const ErrorBag: ErrorContainer = {
	email: "Not a valid email.",
	userName: "Most start with a capital letter",
};
/*

    Index Types: a feature that allows us to create objects that are more 
    flexible in regards to the properties that they may hold. 

    You define an Index Type by using "[]" then any name of your choice ,
    then a ":" and then the type, then another ":" and then the type.

    All other properties of an Interface that has an Index Type, will have
    be of the same type as the Index Type. 


*/
