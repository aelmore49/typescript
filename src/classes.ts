abstract class Department {
	static fiscalYear = 2020;
	//	private name: string;
	protected employees: string[] = [];

	constructor(public id: string, public name: string) {}

	abstract describe(): void;

	addEmployee(employee: string): void {
		this.employees.push(employee);
	}

	printEmployeeInfo(): void {
		console.log({
			"Employee Array Length": this.employees.length,
			"Employee Array:": this.employees,
		});
	}

	static createEmployee(name: string): object {
		return { name: name };
	}
}

class AccountingDepartment extends Department {
	private lastReports: string;
	private static instance: AccountingDepartment;

	get mostRecentReport() {
		if (this.lastReports) {
			return this.lastReports;
		}
		throw new Error("No report found.");
	}

	set mostRecentReport(value: string) {
		this.addReport(value);
	}
	private constructor(id: string, protected reports: string[]) {
		super(id, "Accounting");
		this.lastReports = reports[0];
	}

	static getInstance() {
		if (this.instance) {
			return this.instance;
		}
		this.instance = new AccountingDepartment("d2", []);
		return this.instance;
	}

	addEmployee(name: string) {
		if (name === "Alex") {
			return;
		}
		this.employees.push(name);
	}

	addReport(text: string) {
		this.reports.push(text);
		this.lastReports = text;
	}

	describe() {
		console.log(`Accounting DEPT - ID ${this.id} `);
	}
}

class ITDepartment extends Department {
	admins: string[];
	constructor(id: string, admins: string[]) {
		super(id, "IT");
		this.admins = admins;
	}
	describe() {
		console.log(`IT DEPT - ID ${this.id} `);
	}
}

const accounting = AccountingDepartment.getInstance();
const employee1 = Department.createEmployee("Tim");
const it = new ITDepartment("420", []);

accounting.addEmployee("Doug");
accounting.addEmployee("Alex");
accounting.printEmployeeInfo();
accounting.mostRecentReport = "Quarter 2";
accounting.describe();
it.describe();
/*

	Abstract Classes:
	When you want all extending classes to have access to a
	method on their base class, but the exact implementation of
	said method will depend on the specific inheriting class. 
	In such a situation, you might want to have an empty method
    in your base class, and force all extending classes to add and
    override this method. You can do so by adding the "abstract" keyword in
	front of the method. If you have one method or more with the abstract
	keyword in front of it, you have to make its containing class abstract.
	When you put abstract in front of a method, you have to remove the method's
	curly braces, add a semicolon after its parenthesis and then list its
	return type. 
	Abstract classes cannot be instantiated, only inherited from. 

	Singleton Pattern:
	Pattern in OOP. This pattern is about insuring that you always only have
	one instance of a single class. 
	To make sure that you cannot call new and create an instance of a class, you
	can add private in front of its constructor. 
*/
